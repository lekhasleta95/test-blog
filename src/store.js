import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {
      login: "",
      password: ""
    },
    posts: [
      {
        id: 0,
        title: "Post 1",
        description: "Some description"
      },
      {
        id: 1,
        title: "Post 2",
        description: "Some description"
      },
      {
        id: 2,
        title: "Post 3",
        description: "Some description"
      }
    ]
  },
  mutations: {
    setUser(state, user) {
      state.user = { ...user };
    },
    clearUser(state) {
      state.user = {
        login: "",
        password: ""
      };
    },
    postsFromStorage(state, posts) {
      state.posts = [...posts];
    },
    addPost(state, post) {
      // GET MAX ID FROM POSTS
      let maxId = Math.max.apply(
        Math,
        state.posts.map(function(o) {
          return o.id;
        })
      );
      // GET MAX ID FROM POSTS
      post.id = maxId + 1;

      state.posts = [...state.posts, { ...post }];
    },
    deletePost(state, id) {
      state.posts = state.posts.filter(post => post.id !== id);
    },
    editPost(state, post) {
      let index = state.posts.findIndex(obj => obj.id === post.id);
      state.posts[index].title = post.title;
      state.posts[index].description = post.description;
    }
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    isUser(state) {
      return state.user.login.length > 0;
    },
    getPosts(state) {
      return state.posts;
    }
  },
  actions: {
    setPostsToStorage(context) {
      // ADD POSTS TO SESSION STORAGE
      sessionStorage.setItem("posts", JSON.stringify(context.state.posts));
    },
    addPost(context, post) {
      context.commit("addPost", post);
      context.dispatch("setPostsToStorage");
    },
    deletePost(context, id) {
      context.commit("deletePost", id);
      context.dispatch("setPostsToStorage");
    },
    editPost(context, post) {
      context.commit("editPost", post);
      context.dispatch("setPostsToStorage");
    }
  }
});
